'use strict';
/**
 * @ngdoc function
 * @name sbAdminApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the sbAdminApp
 */
angular.module('sbAdminApp').service("APIService", function ($http)
                                     
                                     
        {
     //var host = "http://zsbio.azurewebsites.net/"; 

   var host = "http://localhost:9443/";
    this.getRegisteredUser = function (index,pageSize) {
        return $http.get(host+"staff/list/1/10")
    }
    this.getAttendanceStatus = function (index,pageSize) {
        return $http.get(host + "staff/bioattendance/" + index+"/"+pageSize)
    }
    this.getUsers = function (index, pageSize) {
        return $http.get(host + "user/list/" + index + "/" + pageSize)
    }
    this.getProductSearchTypes = function () {
        return [
          { id: "0", name: "Please select..." },
          { id: "BadgeName", name: "Badge No" },
          { id: "StaffName", name: "Staff Name" }
        ];
    }
});