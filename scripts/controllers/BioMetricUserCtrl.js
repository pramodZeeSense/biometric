'use strict';
/**
 * @ngdoc function
 * @name sbAdminApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the sbAdminApp
 */
angular.module('sbAdminApp')
  .controller('BioMetricUserCtrl', function ($scope, APIService) {
    getAll(1,10);
      function getAll(index,pageSize) {
        var servCall = APIService.getRegisteredUser(index,pageSize);
        servCall.then(function (d) {
            $scope.reguser = d.data.Table;
        }, function (error) {
            $log.error('Oops! Something went wrong while fetching the data.')
        })
    }
  });
