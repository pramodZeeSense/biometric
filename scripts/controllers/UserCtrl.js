'use strict';
/**
 * @ngdoc function
 * @name sbAdminApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the sbAdminApp
 */
angular.module('sbAdminApp')
  .controller('UserCtrl', function ($scope, APIService) {
      $scope.model = { totalItems: 5 };
      getAll(1, 10);
      function getAll(index, pageSize) {
          var servCall = APIService.getUsers(index, pageSize);
          servCall.then(function (d) {
              $scope.users = d.data.rows;
          }, function (error) {
              $log.error('Oops! Something went wrong while fetching the data.')
          })
      }
  });
