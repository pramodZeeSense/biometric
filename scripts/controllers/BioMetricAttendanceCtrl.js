'use strict';
/**
 * @ngdoc function
 * @name sbAdminApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the sbAdminApp
 */
angular.module('sbAdminApp')
  .controller('BioMetricAttendanceCtrl', function ($scope, APIService) {
      $scope.model = {};
      $scope.search = {};
      $scope.model.productSearchTypes = APIService.getProductSearchTypes();
      $scope.currentPage = 1;
      $scope.maxSize = 5;
      $scope.totalItems = 0;
      $scope.model.attendance = [];
      $scope.limit = 10;
      getAttendanceStatus();
      function getAttendanceStatus() {
          var servCall = APIService.getAttendanceStatus(($scope.currentPage - 1) * $scope.limit, $scope.limit);
          servCall.then(function (d) {
              $scope.totalItems = d.data.total;
              angular.copy(d.data.rows, $scope.model.attendance)
          }, function (error) {
              $log.error('Oops! Something went wrong while fetching the data.')
          })
      }

      $scope.changeDdlClass = function (elemId) {
          toggleDdlClass(elemId);
      }
      //get another portions of data on page changed
      $scope.pageChanged = function () {
          getAttendanceStatus();
      };
      //Set search items for starting and reset.
      var initiateSearchItems = function () {
          $scope.model.pSearchType = { selected: "0" };
          $scope.model.pStatusType = { selected: 0 };
          $scope.model.pPageSizeObj = { selected: 5 };
          //Search parameter.
          $scope.search.pSearchText = "";
          $scope.search.pAvailableFrom = "";
          $scope.search.pAvailableTo = "";

          $scope.errorMessage = undefined;
          $scope.showProductList = false;
      }
      initiateSearchItems();
      //Toggle dropdown CSS classes for placeholder font colors.
      var toggleDdlClass = function (elemId, flag) {
          var elem = document.getElementById(elemId);
          if (elem.selectedIndex == 0 || flag == "reset")
              angular.element(elem).removeClass("control-color").addClass("placeholder-color");
          else
              angular.element(elem).removeClass("placeholder-color").addClass("control-color");
      };
  });
